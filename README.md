## ZeVib converter

To use your own videos with zeCardio, zeVib helps you converting your files to the zeCardio data structure.

### What ?

ZeVib converts from microscope/video files to zecardio data structure.  
It uses [xuggler](http://www.xuggle.com/xuggler/) code for video import (pixelsize to be set by the user)  
and [LOCI bio-formats](https://loci.wisc.edu/software/bio-formats) to deal with some microscopes files.  
We tested our own lei, lif, avi files, but it may not convert all the formats supported by those libraries. Contact us if you need some other formats to be added or feel free to join us by submitting your code.

### How ?

Two options on the main screen:  
![zeVib/doc/convert.png](zeVib/doc/main.png)  
**Batch** will convert all files in a directory and sub directories. It will list all the files found, **if one or some files are avi, it will ask for pixelsize and apply this value to all**

**Convert one File** will convert the file selected, and ask you, if needed, for the pixelsize.  

Accordingly:
**Drag & Drop** your file or directory onto the screen to launch single file or batch conversion. 

### Fill pixelsize if needed

![](zeVib/doc/aviTest.png)  

### Then press convert

![](zeVib/doc/convert.png)  

### It's done

Exit or press back  

![](zeVib/doc/end.png)  

### Open in ZeCardio

A .zecardio file was created, open it with ZeCardio  

![](zeVib/doc/out.png)  

### Faq

**What happens with multiple series/channels in microscope files ?** It will convert all as separate videos.  

**My output is empty / looks strange ?** Your video format may not be supported...yet, send us a sample or convert your file to a known working format first.  

**Output doesn't show all series/channels ?** ZeVib discards non relevant series for ZeCardio (like pictures, bad fps...). 

### To compile

Clone this repository

you need [Processing (3..)](https://processing.org),open it, add 'drop' library from processing menu 'sketch/import library'  
Add xuggle-xuggler-x.x.jar and loci_tools.jar in code directory (load those on [xuggler](http://www.xuggle.com/xuggler/) and [Bio-formats](https://loci.wisc.edu/software/bio-formats) projects website  
Then... run

Please, feel free to contact us for any questions!