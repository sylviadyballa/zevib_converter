void dropEvent(DropEvent theDropEvent) { /// Event on file/directory drag n drop
  messageMain="";

  if (theDropEvent.isFile()) {
    File myFile = theDropEvent.file();

    if (myFile.isDirectory()) 
      folderSelected(myFile);
    else 
    fileSelected(myFile);
  }
}


void folderSelected(File selection) {  // user selected a directory
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {

    try {
      walk(selection.getAbsolutePath());
       if(requirePixelSizeFromUser==true&&pixelSizeFromUser==0)
       
  getPixelSize();
       
    } 
    catch (IOException ex) {
      println(ex.getMessage());
    }
  }
}
void destinationSelected(File selection) {  // user selected a directory
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {

   destinationPath=selection.getAbsolutePath();
  }
}

void fileSelected(File selection) {  // user selected a file
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    if (checkFileExtension(selection)) {
      files2convert.add(new file2convert(selection.getAbsolutePath()));
      if (selection.getAbsolutePath().endsWith("avi")) {
        requirePixelSizeFromUser=true;
        getPixelSize();
      } 
    } else 
    messageMain="This format is not suported yet, Contact ZeClinics for an add-on";
  }
}

void getPixelSize(){ 
  String given=JOptionPane.showInputDialog("Set pixels size for avi (microns)");
  if(given==null ){files2convert = new ArrayList<file2convert>();}
  else
  try{
  pixelSizeFromUser=Float.parseFloat("0"+given);
       }catch(Exception e){getPixelSize();
     println("not a proper float");}
     
    }


boolean checkFileExtension(File selection) {
  return checkFileExtension(selection.getName());
}

boolean checkFileExtension(String selection) {
  boolean found=false;
  for (int s=0; s<acceptedExtensions.length; s++)
  {
    if ( selection.endsWith(acceptedExtensions[s]))found=true;
  }
  println("found extension");
  return found;
}



void walk(String path) throws IOException { //recursive search of microscope files from a directory
  File root = new File(path);
  File[] list = root.listFiles();

  if (list == null) {
    return;
  }

  for (File f : list) {
    if (f.isDirectory()) {
      walk(f.getAbsolutePath());
    } else {
      if (checkFileExtension(f)){
        if (f.getAbsolutePath().endsWith("avi")) requirePixelSizeFromUser=true;
      files2convert.add(new file2convert(""+f.getAbsolutePath()));
      }
    }
  }
 }

String CreationDate(String fileName) {
  return CreationDate(new File( fileName));
} 
String CreationDate(File f) {  
  Path filePath = f.toPath();
  String creationTime="";
  BasicFileAttributes attributes = CreationDateData(f);
  long milliseconds = attributes.creationTime().to(TimeUnit.MILLISECONDS);
  if ((milliseconds > Long.MIN_VALUE) && (milliseconds < Long.MAX_VALUE))
  {
    Date creationDate =
      new Date(attributes.creationTime().to(TimeUnit.MILLISECONDS));
    creationTime="" +(creationDate.getYear() + 1900)+"-"+(creationDate.getMonth() + 1)+"-"+creationDate.getDate();
  }
  return creationTime;
}


BasicFileAttributes CreationDateData(File f) {  
  Path filePath = f.toPath();
  BasicFileAttributes attributes = null;
  try
  {
    attributes =
      Files.readAttributes(filePath, BasicFileAttributes.class);
  }
  catch (IOException exception)
  {
    System.out.println("Exception handled when trying to get file " +
      "attributes: " + exception.getMessage());
  }

  return attributes;
}