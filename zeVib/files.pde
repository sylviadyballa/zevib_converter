import java.lang.String;
import loci.formats.meta.IMetadata;
import loci.formats.services.OMEXMLService;
import loci.common.DateTools;
import com.xuggle.xuggler.Global;
import com.xuggle.xuggler.IContainer;

import com.xuggle.xuggler.IPacket;
import com.xuggle.xuggler.IPixelFormat;
import com.xuggle.xuggler.IStream;
import com.xuggle.xuggler.IStreamCoder;
import com.xuggle.xuggler.ICodec;
import com.xuggle.xuggler.IVideoPicture;
import com.xuggle.xuggler.IVideoResampler;
import com.xuggle.xuggler.Utils;
import ome.units.UNITS;
import ome.units.quantity.ElectricPotential;
import ome.units.quantity.Length;
import ome.units.quantity.Temperature;
import ome.units.quantity.Time;
import ome.xml.model.primitives.Timestamp;
import ome.xml.meta.MetadataRetrieve;
String currentPath="", destinationPath="", messageMain="";
boolean convert=false, removeJunk=true;
int concurrentConversion=0, maxConcurrentConversion=1;
float pixelSizeFromUser=0;
boolean  requirePixelSizeFromUser;
List<file2convert> files2convert = new ArrayList<file2convert>();
String[] acceptedExtensions={"cxd", "lif", "lei", "avi", "czi", "xxx"};

class file2convert {

  String fileNameSource="", path="", fileName="", dirName="";
  String destination="", fileNameOut="";
  String status="", message="", finalMessage="";


  boolean toConvert=true, converting=false, converted=false;

  int percentageDone=0;
  int sizeX;
  int sizeY;
  int sizeZ;
  int sizeC;
  int  sizeT;
  PixelType pixelType;
  int maxp, minp;
  PImage droite;
  int[] histo;
  PositiveFloat physicalSizeX ;
  PositiveFloat physicalSizeY ;
  PositiveFloat physicalSizeZ ;
  float  movieFrameRate;
  double timeIncrement ;

  file2convert(String name) {
    setPaths(name);
    this.destination=path;
  }
  file2convert(String name, String out) {
    setPaths(name);
    this.destination=out;
  }
  void setPaths(String name) {
    this.fileNameSource=name;
    File sourceFile=new File(name);

    this.path=sourceFile.getParentFile() .getAbsolutePath();
    this.fileName=sourceFile.getName();
    this.dirName=fileName.substring(0, sourceFile.getName().lastIndexOf("."));
    this.fileNameOut=dirName;
    println("fileNameSource "+fileNameSource);
    println("path "+path);
    println("fileName "+fileName);
    println("dirName "+dirName);
    println("fileNameOut "+fileNameOut);
    println("destination "+destination);
    /*
    fileNameSource myFolder/myFile.avi
     path myFolder
     fileName myFile.avi
     dirName myFile
     fileNameOut myFile
     */
  }

  void display() {

    textAlign(RIGHT, CENTER);
    fill(200, 200, 200);
    if (!currentPath.equals(path)) {
      currentPath=path;
      translate(0, 20);
      text(path, -1000, 0, 1300, 20);
      if (!destinationPath.equals("")) {
        textAlign(LEFT, CENTER);
        text(" > "+destinationPath, 300, 0, 1300, 20);
      }
      translate(0, 20);
    }

    if (toConvert)fill(255); 
    else  fill(200);

    text(fileName, -1000, 0, 1400, 20);

    if (convert)
      if (concurrentConversion<maxConcurrentConversion&&toConvert) {
        concurrentConversion++;
        if (!destinationPath.equals(""))destination=destinationPath;
        //if (fileName.endsWith("lif")||fileName.endsWith("lei")||fileName.endsWith("cxd")) { //leica camera
        if (fileName.endsWith("lif")||fileName.endsWith("lei")||fileName.endsWith("cxd")||fileName.endsWith("czi")) { //leica camera
          callLoci thread2 = new callLoci();  
          thread2.start();
        } else
          if (fileName.endsWith("avi")) {
            callXuggler thread2 = new callXuggler();  
            thread2.start();
          }
      }

    if (converting) {
      stroke(255);
      noFill();
      rect(450, 8, 200, 7);

      fill(250- percentageDone, 150+ percentageDone, 50);
      rect(450, 8, 2*percentageDone, 7);
      fill(255);
      textAlign(LEFT);
      text(message, 480, 32);
    }
    if (converted) {
      tint(0, 255, 0);
      image(picto_checked, 450, 3, 12, 12);
      noTint();
      text(finalMessage, 480, 12);
    }

    translate(0, 20);
  }

  public class callLoci extends Thread {


    ServiceFactory factory;
    OMEXMLService service;
    IMetadata meta;

    // create format reader
    IFormatReader reader;

    public callLoci ( ) {

      //   this.source=source;
    }


    public void run ()
    {

      try {
        int seriesCount;
        converting=true;


        factory = new ServiceFactory();
        service = factory.getInstance(OMEXMLService.class);
        meta = service.createOMEXMLMetadata();

        // create format reader
        reader = new ImageReader();
        reader.setMetadataStore(meta);

        // initialize file
        System.out.println("Initializing " + fileNameSource);
        reader.setId(fileNameSource);

        seriesCount = reader.getSeriesCount();
        System.out.println(seriesCount);
        XML xml=new XML("Series");
        for (int serie=0; serie<seriesCount; serie++) {
          histo= new int[65537];
          //  for (int serie=2; serie<3; serie++) {
          percentageDone=0;
          message="Converting serie "+(serie+1)+"/"+seriesCount;
          boolean toProcess=true;
          reader.setSeries(serie);

          sizeX = reader.getSizeX();

          sizeY = reader.getSizeY();
          sizeZ = reader.getSizeZ();
          sizeC = reader.getSizeC();
          sizeT = reader.getSizeT();

          pixelType=meta.getPixelsType(serie);

          /*
          System.out.println();
           System.out.println("Pixel dimensions:");
           System.out.println("\tWidth = " + sizeX);
           System.out.println("\tHeight = " + sizeY);
           System.out.println("\tFocal planes = " + sizeZ);
           System.out.println("\tChannels = " + sizeC);
           System.out.println("\tTimepoints = " + sizeT);
           */
          physicalSizeX = meta.getPixelsPhysicalSizeX(serie);
          physicalSizeY = meta.getPixelsPhysicalSizeY(serie);
          physicalSizeZ = meta.getPixelsPhysicalSizeZ(serie);

          if (fileName.endsWith("cxd")) {  
            try {
              ServiceFactory serviceFactory = new ServiceFactory();
              OMEXMLService omexmlService =
                serviceFactory.getInstance(OMEXMLService.class);


              String xmlOME=omexmlService.getOMEXML(meta);

              String[] xmls=split(xmlOME.substring(xmlOME.lastIndexOf("<Plane"), xmlOME.lastIndexOf(" TheZ")), "\"");

              movieFrameRate=(float)sizeT/Float.parseFloat(xmls[1]);
            }
            catch (Exception e) {
            }
          } else 
          if (fileName.endsWith("czi")) {  
            try {


              Double timeIncrement =(meta.getPlaneDeltaT(serie, sizeT-1)- meta.getPlaneDeltaT(serie, 0))/((float)sizeT-1.0);



              movieFrameRate=(float)(1.0/timeIncrement);
            }
            catch (Exception e) {
            }
          } else if (fileName.endsWith("lif")||fileName.endsWith("lei")) {
            //} else if (fileName.endsWith("lif")||fileName.endsWith("lei")) {
            println("c");


            Object o = reader.getSeriesMetadataValue("Image|ATLCameraSettingDefinition|CompleteTime");//Image|ATLCameraSettingDefinition|CompleteTime
            println("d");

            movieFrameRate=(float)sizeT/(Float.parseFloat(o.toString()));
            println("d");
          }
          System.out.println("Initializing a" +0 +" ");
          //double timeIncrement = meta.getPixelsTimeIncrement(serie);
          System.out.println();
          System.out.println("Physical dimensions:");
          System.out.println("\tX spacing = " + physicalSizeX + " microns");
          System.out.println("\tY spacing = " + physicalSizeY + " microns");
          System.out.println("\tZ spacing = " + physicalSizeZ + " microns");
          System.out.println("\tFramrate = " + movieFrameRate + " fps");
          System.out.println("\tpixelType = " + pixelType + " ");

          minp=1000000;
          maxp=0;
          int nboctet=2, signe=-1;

          if (pixelType==ome.xml.model.enums.PixelType.UINT8) { 
            nboctet=1;
            signe=-1;
          }
          if (pixelType==ome.xml.model.enums.PixelType.UINT16) { 
            nboctet=2;
            signe=-1;
          }
          if (pixelType==ome.xml.model.enums.PixelType.UINT32) { 
            nboctet=4;
            signe=-1;
          }
          if (pixelType==ome.xml.model.enums.PixelType.INT8) { 
            nboctet=1;
            signe=1;
          }
          if (pixelType==ome.xml.model.enums.PixelType.INT16) { 
            nboctet=2;
            signe=1;
          }
          if (pixelType==ome.xml.model.enums.PixelType.INT32) { 
            nboctet=4;
            signe=1;
          }

          if (sizeX==0||sizeY==0||sizeT<=2)toProcess=false; // series to discard, probably junk
          if (toProcess||!removeJunk) {

            if (seriesCount>1)   fileNameOut=dirName+"_s"+serie;




            println(path+File.separator+dirName+File.separator+fileName);
            boolean   mk = new File(destination+File.separator+dirName).mkdir(); ///creating main dirctory output
            mk = new File(destination+File.separator+dirName+File.separator+fileNameOut).mkdir();  ///creating actual serie directory 


            XML newChild = xml.addChild("Video");
            newChild.setContent("");
            newChild.setString("name", fileNameOut);
            newChild.setString("path", dirName);
            newChild.setString("addDate", year() + "-"  + nf(month(), 2) + "-"  + nf(day(), 2)+ " "  + nf(hour(), 2)+ ":"  + nf(minute(), 2));
            newChild.setString("SourcePath", path);
            newChild.setString("SourceFile", fileName);
            newChild.setString("FileDate", CreationDate(fileNameSource));
            newChild.setString("AcquisitionDate", meta.getImageAcquisitionDate(0).getValue());
            newChild.setString("FrameRate", Float.toString(movieFrameRate));
            newChild.setString("nbFrames", ""+sizeT);
            newChild.setString("x", ""+sizeX);
            newChild.setString("y", ""+sizeY);
            newChild.setString("status", "Ready");
            newChild.setString("startingFrame", "0");
            newChild.setString("endingFrame", ""+(sizeT));
            newChild.setFloat("Contrast", 100.0);
            newChild.setFloat("Alpha", 0.0);
            newChild.setInt("Enabled", 0);
            newChild.setInt("Processed", 0);
            newChild.setFloat("PixelSize", Float.parseFloat(physicalSizeX.toString()));

            int[] averageFrameInt;//int newFrame = 0;
            byte[] averageFrame;//int newFrame = 0;
            byte[] minFrame;//int newFrame = 0;
            byte[] maxFrame;//int newFrame = 0;
            int[][] downsample;
            int imgPixels=sizeX*sizeY;
            int downScale=16;
            averageFrameInt=new int[imgPixels];
            downsample=new int[(sizeX/downScale)* (sizeY/downScale)][sizeT];
            averageFrame=new byte[imgPixels];
            //  videoBuffer=new byte[imgPixels*sizeT];
            minFrame=new byte[imgPixels];
            maxFrame=new byte[imgPixels];
            for (int x=0; x<imgPixels; x++) {
              minFrame[x]=(byte)255;
            }

            System.out.println("Initializing d"+sizeX+" "+ sizeT+" "+pixelType);
            println("pixelType"+pixelType);
            if (pixelType==ome.xml.model.enums.PixelType.UINT16) {
              //   println("pixelType"+pixelType);


              for (int p=0; p<65535; p++) { // question: why 4096  /// changed to 65535, 4096 was for 12 bit cxd file, to handle 16 bit, reset the histogram
                histo[p]=0;
              }

              //  reader.setSeries(selectedSerie);
              System.out.println("Initializing de"+sizeX+" "+ sizeT+" "+FormatTools.getPlaneSize(reader));
              //  movieInfo = "Building conversion histogram, wait please ";
              message+="\n\rReading video";
              byte[] plane = new byte[FormatTools.getPlaneSize(reader)];
              for (int t=0; t<sizeT; t+=5) { // question: why t+=5 ? just checking if can access the data?
                // println(plane.length+" "+(sizeX*sizeY)+" "+pixelType);
                try {
                  //  println(indx+" "+sizeZ+" " +sizeC);
                  reader.openBytes(t, plane);
                }
                catch (IOException e) {
                  System.err.println("Failed to convert image #" );
                  e.printStackTrace();
                }
                catch (FormatException e) {
                  System.err.println("Failed to convert image f" );
                  e.printStackTrace();
                }


                for (int pix=0; pix<sizeX*sizeY; pix++) {

                  int p = (((plane[pix*2+1]& 0x000000ff) << 8) & 0x0000ff00) | (plane[pix*2] & 0x000000ff);
                  histo[p]++;
                }
                percentageDone = t*20/sizeT; // progression bar 0-20%
              }
              int nb=0;
              for (int p=0; p<65535; p++) {

                nb+=histo[p];

                if (nb>=(((sizeX*sizeY)/5)*sizeT)/100000) {
                  //  if (nb>=1) {
                  minp=p;
                  p=65535;
                }
              }

              nb=0;
              for (int p=65535; p>0; p--) {
                nb+=histo[p];
                if (nb>=(((sizeX*sizeY)/5)*sizeT)/100000) {
                  //    if (nb>=1) {
                  maxp=p;
                  p=0;
                }
              }

              println("minp="+minp+" maxp="+maxp);
            }
            //readframe=0;
            message +="\n\rConverting data, wait please ";

            try {  


              FileOutputStream wrBuf = new FileOutputStream(destination+File.separator+dirName+File.separator+fileNameOut+File.separator+fileNameOut+".vib");
              for (int t=0; t<sizeT; t++) {
                //  println("a"+t);
                byte[] pixelsBuffer = readSlice(0, 0, serie, t, reader);
                wrBuf.write(pixelsBuffer);
                int  downScaleS=downScale*downScale;
                for (int x=0; x<sizeX; x++) {

                  for (int y=0; y<sizeY; y++) {
                    int position=x+(y*sizeX);
                    byte p=pixelsBuffer[(position)];

                    // videoBuffer[position+offsetBuffer]=p;
                    minFrame[position]=(byte)min(p&255, minFrame[position]&255);
                    maxFrame[position]=(byte)max(p&255, maxFrame[position]&255);
                    averageFrameInt[position]+=p&255;
                  }
                }

                float nbp=(float)(downScale*downScale);
                for (int x=0; x<sizeX/downScale; x++) {
                  int xDownscaled=x*downScale;
                  for (int y=0; y<sizeY/downScale; y++) {
                    int yDownscaled=y*downScale;
                    int pixelSum=0;
                    for (int dx=0; dx<downScale; dx++) {
                      int xposition=xDownscaled+dx;
                      for (int dy=0; dy<downScale; dy++) {
                        int position=xposition+(yDownscaled+dy)*sizeX;
                        byte p=pixelsBuffer[(position)];
                        pixelSum+=p;
                      }
                    }
                    downsample[x+y*(sizeX/downScale)][t]=(byte)(pixelSum/downScaleS);
                  }
                }
                percentageDone=20+(t*60/sizeT); ///20-80 %
                // readframe++;
              }

              println("calculate downsampled"); 
              //rwChannel.close();
              wrBuf.close(); // question: maybe is not downsampled, but because is 16 bit??? not sure about this....
            } 
            catch (IOException e) {
              e.printStackTrace();
            }



            message+="\n\rWriting to disk...wait please";

            try { 
              java.io.RandomAccessFile file = new java.io.RandomAccessFile(destination+File.separator+dirName+File.separator+fileNameOut+File.separator+fileNameOut+".minFrame", "rw");
              FileChannel rwChannel = file.getChannel();

              ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, minFrame.length);
              wrBuf.put(minFrame);
              rwChannel.close();
              file.close();
            } 
            catch (IOException e) {
              e.printStackTrace();
            }  
            //  println("3");
            percentageDone=83; ///85 %

            try { 
              java.io.RandomAccessFile file = new java.io.RandomAccessFile(destination+File.separator+dirName+File.separator+fileNameOut+File.separator+fileNameOut+".maxFrame", "rw");
              FileChannel rwChannel = file.getChannel();

              ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, maxFrame.length);
              wrBuf.put(maxFrame);
              rwChannel.close();
              file.close();
            } 
            catch (IOException e) {
              e.printStackTrace();
            }
            percentageDone=86; ///85 %

            averageFrame=new byte[imgPixels];
            for (int x=0; x<imgPixels; x++) {
              averageFrame[x]=(byte)(max(0, averageFrameInt[x]/sizeT));
            }
            // println("4");

            try {  
              java.io.RandomAccessFile file = new java.io.RandomAccessFile(destination+File.separator+dirName+File.separator+fileNameOut+File.separator+fileNameOut+".averageFrame", "rw");
              FileChannel rwChannel = file.getChannel();
              ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, averageFrame.length);
              wrBuf.put(averageFrame);
              rwChannel.close();  
              file.close();
            } 
            catch (IOException e) {
              e.printStackTrace();
            }
            //   println("5");
            percentageDone=90; ///85 %

            try {  
              java.io.RandomAccessFile file = new java.io.RandomAccessFile(destination+File.separator+dirName+File.separator+fileNameOut+File.separator+fileNameOut+".downsampled", "rw");

              FileChannel rwChannel = file.getChannel();
              ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, (sizeX/downScale)* (sizeY/downScale)* sizeT*4);
              //   downsample=new int[(sizeX/downScale)* (sizeY/downScale)][sizeT];
              for (int x=0; x<(sizeX/downScale)* (sizeY/downScale); x++) {
                for (int f=0; f<sizeT; f++) {
                  wrBuf.putInt(downsample[x][f]);
                }
              }
              rwChannel.close();
              file.close();
            } 
            catch (IOException e) {
              e.printStackTrace();
            }
            percentageDone=93; ///85 %
          }
        }
        try { 
          saveXML(xml, destination+File.separator+dirName+".zecardio");
        } 
        catch(Exception e) {
          messageMain="I can't save files, check permissions";
        }
        concurrentConversion--;
        toConvert=false;
        converting=false;
        converted=true;
      }
      catch (IOException e) {
        System.out.println("fail2");

        println(e);
      }
      catch (DependencyException e) {
        System.out.println("fail3");

        println(e);
      }
      catch (ServiceException e) {
        System.out.println("fail4");

        println(e);
      }
      catch (Exception e) {
        //System.out.println("fail4");
        System.out.println("fail5");

        println(e);
        exit();
      }
    }


    byte[] readSlice(int askedSlice, int channel, int selectedSerie, int askedTime, IFormatReader reader) {

      // int selectedSerie=0;

      // int maxp=0, minp=100000;
      reader.setSeries(selectedSerie);


      byte[] plane = new byte[FormatTools.getPlaneSize(reader)];
      byte[] out = new byte[sizeX*sizeY];
      // println(plane.length+" "+(sizeX*sizeY)+" "+pixelType);
      try {
        //  println(indx+" "+sizeZ+" " +sizeC);
        reader.openBytes(askedTime, plane);
      }
      catch (IOException e) {
        System.err.println("Failed to convert image #" );
        e.printStackTrace();
      }
      catch (FormatException e) {
        System.err.println("Failed to convert image #" );
        e.printStackTrace();
      }


      if (pixelType==ome.xml.model.enums.PixelType.UINT16) {
        //   println("pixelType"+pixelType);
        //   println("16 "+minp+" "+maxp);
        for (int pix=0; pix<sizeX*sizeY; pix++) {

          int p = (((plane[pix*2+1]& 0x000000ff) << 8) & 0x0000ff00) | (plane[pix*2] & 0x000000ff);
          p=(255*(p-minp))/(maxp-minp);
          out[pix]=(byte)min(255, max(0, p));
        }
        return out;
      } else {


        return plane;

        /*   for (int pix=0; pix<sizeX*sizeY; pix++) {
         
         
         out[pix]=(byte)(plane[pix]);
         }*/
      }

      // return out;
    }


    PImage readSlice(int askedSlice, int channel, int askedTime, PImage droite, IFormatReader reader) {

      int selectedSerie=0;

      reader.setSeries(selectedSerie);


      byte[] plane = new byte[FormatTools.getPlaneSize(reader)];
      println(plane.length+" "+(sizeX*sizeY)+" "+pixelType);
      try {
        //  println(indx+" "+sizeZ+" " +sizeC);
        reader.openBytes(askedTime, plane);
      }
      catch (IOException e) {
        System.err.println("Failed to convert image #" );
        e.printStackTrace();
      }
      catch (FormatException e) {
        System.err.println("Failed to convert image #" );
        e.printStackTrace();
      }


      if (pixelType==ome.xml.model.enums.PixelType.UINT16) {
        for (int p=0; p<4096; p++) {
          histo[p]=0;
        }
        if (maxp==0) {

          for (int pix=0; pix<droite.pixels.length; pix++) {
            //int p=    ( ((plane[pix*2+1]&255)<<8)+(plane[pix*2]&255));
            //int p= (plane[pix*2]&255)+256*(plane[pix*2+1]&255);
            //int p= (plane[pix*2+1]&255);

            int p = (((plane[pix*2+1]& 0x000000ff) << 8) & 0x0000ff00) | (plane[pix*2] & 0x000000ff);
            histo[p]++;
          }
          for (int p=0; p<4096; p++) {
            //     print(p+"="+histo[p]+" - ");
          }
          int nb=0;
          for (int p=0; p<4096; p++) {

            nb+=histo[p];
            //  if (nb>=1) {
            if (nb>=sizeX*sizeY/10000) {
              minp=p;
              p=4096;
            }
          }

          nb=0;
          for (int p=4095; p>0; p--) {
            nb+=histo[p];
            if (nb>=(sizeX*sizeY)/10000) {
              //   if (nb>=1) {
              maxp=p;
              p=0;
            }
          }
        }


        println("16 "+minp+" "+maxp);
        for (int pix=0; pix<droite.pixels.length; pix++) {

          int p=    ( (256*(plane[pix*2+1]&255))+(plane[pix*2]&255));
          p=(255*(p-minp))/(maxp-minp);
          droite.pixels[pix]=color(min(255, max(0, p)));
        }
      } else {


        for (int p=0; p<256; p++) {
          histo[p]=0;
        }
        if (maxp==0) {

          for (int pix=0; pix<droite.pixels.length; pix++) {
            //int p=    ( ((plane[pix*2+1]&255)<<8)+(plane[pix*2]&255));
            //int p= (plane[pix*2]&255)+256*(plane[pix*2+1]&255);
            //int p= (plane[pix*2+1]&255);

            int p = plane[pix] & 0x000000ff;
            histo[p]++;
          }
          for (int p=0; p<256; p++) {
            //     print(p+"="+histo[p]+" - ");
          }
          int nb=0;
          for (int p=0; p<256; p++) {

            nb+=histo[p];
            //  if (nb>=1) {
            if (nb>=sizeX*sizeY/10000) {
              minp=p;
              p=256;
            }
          }

          nb=0;
          for (int p=255; p>0; p--) {
            nb+=histo[p];
            if (nb>=(sizeX*sizeY)/10000) {
              //   if (nb>=1) {
              maxp=p;
              p=0;
            }
          }
        }

        for (int pix=0; pix<droite.pixels.length; pix++) {

          int p=plane[pix];
          p=(255*(p-minp))/(maxp-minp);
          droite.pixels[pix]=color(p);
        }
      }
      droite.updatePixels();
      return droite;
    }






    public void start ()
    {    
      super.start();
    }
    public void quit()
    {
      System.out.println("Quitting.");
      interrupt(); // in case the thread is waiting. . .
    }
  }


  //LOCI CXD




  ///// Xuggler

  public class callXuggler extends Thread {

    IContainer container;
    IPacket packet;


    public callXuggler ( ) {
      converting=true;
    }

    public void run ()
    {


      if (!IVideoResampler.isSupported(
        IVideoResampler.Feature.FEATURE_COLORSPACECONVERSION))
        throw new RuntimeException( "you must install the GPL version of Xuggler (with IVideoResampler support) for this demo to work");

      // create a Xuggler container object
      container = IContainer.make();

      // open up the container
      if (container.open(fileNameSource, IContainer.Type.READ, null) < 0)
      {  
        throw new IllegalArgumentException("could not open file: " + fileNameSource);
      }

      // query how many streams the call to open found
      int numStreams = container.getNumStreams();

      // and iterate through the streams to find the first video stream
      int videoStreamId = -1;
      IStreamCoder videoCoder = null;
      int      movieFrames=0;
      for (int i = 0; i < numStreams; i++)
      {
        // find the stream object
        IStream stream = container.getStream(i);

        // get the pre-configured decoder that can decode this stream;
        IStreamCoder coder = stream.getStreamCoder();

        if (coder.getCodecType() == ICodec.Type.CODEC_TYPE_VIDEO)
        {
          videoStreamId = i;
          videoCoder = coder;


          movieFrameRate=(float)stream.getFrameRate().getDouble() ;


          movieFrames=(int)stream.getNumFrames();
          sizeT=movieFrames;
          println("------"+movieFrameRate+" "+movieFrames);

          break;
        }
      }

      if (videoStreamId == -1)
        throw new RuntimeException("could not find video stream in container: "+fileNameSource);
      println( "videoStreamId"+videoStreamId);
      // Now we have found the video stream in this file.  Let's open up
      // our decoder so it can do work

      if (videoCoder.open(null, null) < 0)
        throw new RuntimeException(
          "could not open video decoder for container: " + fileNameSource);

      IVideoResampler resampler = null;
      if (videoCoder.getPixelType() != IPixelFormat.Type.BGR24)
      {
        // if this stream is not in BGR24, we're going to need to
        // convert it.  The VideoResampler does that for us.

        resampler = IVideoResampler.make(
          videoCoder.getWidth(), videoCoder.getHeight(), IPixelFormat.Type.BGR24, 
          videoCoder.getWidth(), videoCoder.getHeight(), videoCoder.getPixelType());
        if (resampler == null)
          throw new RuntimeException(
            "could not create color space resampler for: " + fileNameSource);
      }

      //println("videoCoder.getWidth() "+videoCoder.getWidth());
      //println("videoCoder.getHeight() "+videoCoder.getHeight());
      //println("videoCoder.getPixelType() "+videoCoder.getPixelType());
      message = "Converting video, Please wait ";
      sizeX=videoCoder.getWidth();
      sizeY=videoCoder.getHeight();
      int imgPixels=sizeX*sizeY;

      PImage  img=new PImage(sizeX, sizeY, PConstants.RGB);
      int downScale=1;
      int[] averageFrameInt=new int[imgPixels];
      int[][] downsample=new int[(sizeX/downScale)* (sizeY/downScale)][movieFrames];
      byte[] averageFrame=new byte[imgPixels];
      byte[] videoBuffer=new byte[imgPixels*movieFrames];
      byte[] minFrame=new byte[imgPixels];
      byte[] maxFrame=new byte[imgPixels];
      for (int x=0; x<imgPixels; x++) {
        minFrame[x]=(byte)255;
      }

      PImage frequencyImage=createImage(sizeX/downScale, sizeY/downScale, RGB);
      int[] velocity=new int[movieFrames];

      packet = IPacket.make();
      IConverter converter = ConverterFactory.createConverter(
        "XUGGLER-BGR-24", IPixelFormat.Type.BGR24, 
        sizeX, sizeY);

      boolean   mk = new File(destination+File.separator+dirName).mkdir();
      mk = new File(destination+File.separator+dirName+File.separator+fileNameOut).mkdir();


      XML xml=new XML("Series");
      XML newChild = xml.addChild("Video");
      newChild.setContent("");
      newChild.setString("name", fileNameOut);
      newChild.setString("path", dirName);
      newChild.setString("SourcePath", path);
      newChild.setString("SourceFile", fileName);
      newChild.setString("addDate", year() + "-"  + nf(month(), 2) + "-"  + nf(day(), 2)+ " "  + nf(hour(), 2)+ ":"  + nf(minute(), 2));


      newChild.setString("FileDate", CreationDate(fileNameSource));

      newChild.setString("FrameRate", Float.toString(movieFrameRate));
      newChild.setString("nbFrames", ""+sizeT);
      newChild.setString("x", ""+sizeX);
      newChild.setString("y", ""+sizeY);
      newChild.setString("status", "Ready");
      newChild.setString("startingFrame", "0");
      newChild.setString("endingFrame", ""+(sizeT));
      newChild.setFloat("Contrast", 100.0);
      newChild.setFloat("Alpha", 0.0);
      newChild.setInt("Enabled", 0);
      newChild.setInt("Processed", 0);
      newChild.setFloat("PixelSize", pixelSizeFromUser);

      try { 
        saveXML(xml, destination+File.separator+dirName+".zecardio");
      } 
      catch(Exception e) {
        messageMain="I can't save files, check permissions";
      }
      /////////////  /////////////  /////////////  /////////////  /////////////
      /////////////  /////////////  /////////////  /////////////  /////////////
      int readframe=0;


      while (container.readNextPacket (packet) >= 0 )
      {//println("done"+readframe);

        // Now we have a packet, let's see if it belongs to our video strea

        if (packet.getStreamIndex() == videoStreamId)
        {
          // We allocate a new picture to get the data out of Xuggle

          IVideoPicture picture = IVideoPicture.make(videoCoder.getPixelType(), 
            videoCoder.getWidth(), videoCoder.getHeight());

          int offset = 0;
          while (offset < packet.getSize ())
          {
            // Now, we decode the video, checking for any errors.

            int bytesDecoded = videoCoder.decodeVideo(picture, packet, offset);
            //   println("bytesDecoded"+bytesDecoded);
            if (bytesDecoded < 0) {
              println("got error decoding video in: " + fileNameSource+" "+offset+" "+packet.getSize());


              offset = packet.getSize();
            }
            //            throw new RuntimeException("got error decoding video in: " + source);
            else {
              offset += bytesDecoded;

              // Some decoders will consume data in a packet, but will not
              // be able to construct a full video picture yet.  Therefore
              // you should always check if you got a complete picture from
              // the decode.

              if (picture.isComplete())
              {
                IVideoPicture newPic = picture;

                // If the resampler is not null, it means we didn't get the
                // video in BGR24 format and need to convert it into BGR24
                // format.

                if (resampler != null)
                {
                  // we must resample
                  newPic = IVideoPicture.make(
                    resampler.getOutputPixelFormat(), picture.getWidth(), 
                    picture.getHeight());
                  if (resampler.resample(newPic, picture) < 0)
                    throw new RuntimeException(
                      "could not resample video from: " + fileNameSource);
                }

                if (newPic.getPixelType() != IPixelFormat.Type.BGR24)
                  throw new RuntimeException(
                    "could not decode video as BGR 24 bit data in: " + fileNameSource);

                // convert the BGR24 to an Java buffered image
                BufferedImage javaImage = converter.toImage(newPic);


                byte[] pixelsBuffer = ((DataBufferByte) javaImage.getRaster().getDataBuffer()).getData();
                int offsetBuffer=readframe*imgPixels, downScaleS=downScale*downScale;
                ;
                float nbp=(float)(downScale*downScale);
                for (int x=0; x<sizeX/downScale; x++) {
                  int xDownscaled=x*downScale;
                  for (int y=0; y<sizeY/downScale; y++) {
                    int yDownscaled=y*downScale;
                    int pixelSum=0;
                    for (int dx=0; dx<downScale; dx++) {
                      int xposition=xDownscaled+dx;
                      for (int dy=0; dy<downScale; dy++) {
                        int position=xposition+(yDownscaled+dy)*sizeX;
                        byte p=(byte)max(pixelsBuffer[3*(position)+1]&255, pixelsBuffer[3*(position)]&255);
                        pixelSum+=p;
                        videoBuffer[position+offsetBuffer]=p;
                        minFrame[position]=(byte)min(p&255, minFrame[position]&255);
                        maxFrame[position]=(byte)max(p&255, maxFrame[position]&255);
                        averageFrameInt[position]+=p&255;
                      }
                    }
                    downsample[x+y*(sizeX/downScale)][readframe]=(byte)(pixelSum/downScaleS);
                  }
                }




                percentageDone++;
                percentageDone = readframe*80/sizeT; 
                readframe++;
              }
            }
          }
        } else
        {
          // This packet isn't part of our video stream, so we just
          // silently drop it.
          do {
          } while (false);
        }
      }


      message="Writing to disk...Please Wait";
      // println("Writing to disk...Please Wait");
      try {  

        java.io.RandomAccessFile file = new java.io.RandomAccessFile(destination+File.separator+dirName+File.separator+fileNameOut+File.separator+fileNameOut+".vib", "rw");
        FileChannel rwChannel = file.getChannel();
        ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, videoBuffer.length);
        wrBuf.put(videoBuffer);
        rwChannel.close();
        file.close();
      } 
      catch (IOException e) {
        println("xug 316");
        e.printStackTrace();
      }
      percentageDone=90;
      float maxp = 0.0f;
      float minp = 10000.0f;

      for (int x=0; x < imgPixels; x++) {
        int p = maxFrame[x] & 255;
        maxp = max((float)maxp, (float)p);
        minp = min((float)minp, (float)p);
      }

      try { 
        java.io.RandomAccessFile file = new java.io.RandomAccessFile(destination+File.separator+dirName+File.separator+fileNameOut+File.separator+fileNameOut+".minFrame", "rw");
        FileChannel rwChannel = file.getChannel();

        ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, minFrame.length);
        wrBuf.put(minFrame);
        rwChannel.close();
        file.close();
      } 
      catch (IOException e) {
        println("xug 328");
        e.printStackTrace();
      }  
      try { 
        java.io.RandomAccessFile file = new java.io.RandomAccessFile(destination+File.separator+dirName+File.separator+fileNameOut+File.separator+fileNameOut+".maxFrame", "rw");
        FileChannel rwChannel = file.getChannel();

        ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, maxFrame.length);
        wrBuf.put(maxFrame);
        rwChannel.close();
        file.close();
      } 
      catch (IOException e) {
        println("xug 340");
        e.printStackTrace();
      }
      averageFrame=new byte[imgPixels];
      for (int x=0; x<imgPixels; x++) {
        averageFrame[x]=(byte)(averageFrameInt[x]/movieFrames);
      }

      try {  
        java.io.RandomAccessFile file = new java.io.RandomAccessFile(destination+File.separator+dirName+File.separator+fileNameOut+File.separator+fileNameOut+".averageFrame", "rw");
        FileChannel rwChannel = file.getChannel();
        ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, averageFrame.length);
        wrBuf.put(averageFrame);
        rwChannel.close();  
        file.close();
      } 
      catch (IOException e) {
        println("xug 356");
        e.printStackTrace();
      }

      try {  



        java.io.RandomAccessFile file = new java.io.RandomAccessFile(destination+File.separator+dirName+File.separator+fileNameOut+File.separator+fileNameOut+".downsampled", "rw");

        FileChannel rwChannel = file.getChannel();
        ByteBuffer wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, 0, (sizeX/downScale)* (sizeY/downScale)* movieFrames*4);
        for (int x=0; x<(sizeX/downScale)* (sizeY/downScale); x++) {
          for (int f=0; f<movieFrames; f++) {
            wrBuf.putInt(downsample[x][f]);
          }
        }
        rwChannel.close();
        file.close();
      } 
      catch (IOException e) {
        println("xug 377");
        e.printStackTrace();
      }
      concurrentConversion--;
      toConvert=false;
      converting=false;
      converted=true;
    }



    public void start ()
    {    
      super.start();
    }
    public void quit()
    {
      System.out.println("Quitting.");
      interrupt(); // in case the thread is waiting. . .
    }
  }
}