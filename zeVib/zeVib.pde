///// Converter from microscope/video files to zecardio data structure
///// It use xuggler code for video import (pixelsize to be set by the user)
///// and loci to deal with some microscopes files
///// set and Tested for our own lif, avi files, it may not convert all the format supported by those libraries
///// contact us if you need some others format to be added or feel free to join this developpement on https://bitbucket.org/sylviadyballa/zevib_converter
/////

import java.util.List;
import java.util.*;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile.*;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

import java.io.RandomAccessFile.*;
import java.util.concurrent.TimeUnit;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import  java.lang.String;
import static java.lang.Integer.MAX_VALUE;
import static java.lang.System.out;
import static java.nio.channels.FileChannel.MapMode.READ_ONLY;
import static java.nio.channels.FileChannel.MapMode.READ_WRITE;
import java.awt.image.DataBufferByte;
import java.awt.image.BufferedImage;
import javax.swing.JOptionPane;

import drop.*;


String urlZeClinics="", urlSource="https://bitbucket.org/sylviadyballa/zevib_converter";




SDrop drop;



int listOffset=0;
PImage picto_checked, logo;

XML toDo;
void setup() {
  try {
    toDo = loadXML(sketchPath(File.separator+"toDO.zevib"));
  }
  catch(Exception e) {
    println("nothing to do");
  }
  if (toDo!=null) {
    XML[] task=toDo.getChildren("task");
    for (int i=0; i< task.length; i++) {
      files2convert.add(new file2convert(task[i].getString("filename"),task[i].getString("destination")));
    }
    convert=true;
  }
  
  size(700, 800);

  picto_checked=loadImage(dataPath("checked.png"));
  logo=loadImage("logo.png");
  // frameRate(12);
  drop = new SDrop(this);
}






void draw() {  

  background(0);
  fill(255);
  textSize(16);
  textAlign(CENTER);
  text(messageMain, width/2, 20);
  image(logo, 0, 00, logo.width/2, logo.height/2);
  linkButton("Info", urlSource, width-35, 10, 40, "");
  if (files2convert.isEmpty()) {


    if (button(width/2 -90, 150, 180, " Convert one file ", "Select a microscope file")) {
      mousePressed=false;
      messageMain="";
      selectInput("Select a file or directory to process:", "fileSelected");
    }
    if (button(width/2 -90, 80, 180, " Batch conversion ", "Select a directory")) {
      mousePressed=false;
      messageMain="";
      selectFolder("Select a folder to process:", "folderSelected");
    }
  } else {

    textSize(14);

    boolean something2do=false;
    pushMatrix();
    translate(0, 180);
    if(listOffset-20>0)
    translate(0, -20*(listOffset-20));
    currentPath="";
    try{
      int i=0;
      for (file2convert currentFile : files2convert) {
i++;
    // if(i>listOffset-28)
     currentFile.display();
      if (currentFile.converting||currentFile.toConvert)
      {
        something2do=true;
      }
      if (currentFile.converting)
      {
      listOffset=i;
      }
    
    }
    }catch(Exception e){/* we re drawing while files are added*/}
    popMatrix();
    if (convert==false) {
      if ( something2do) {
        if (requirePixelSizeFromUser&&pixelSizeFromUser==0) {
        } else 
        convert=button(width/2 -90, 150, 180, " Convert ", "");
        if(destinationPath.equals(""))if (buttonText(width/2+ 190, 200, 180, " Change destination ", "")) {
          mousePressed=false;
          messageMain="";
          selectFolder("Select destination folder:", "destinationSelected");
        }
        if (button(width/2 -90, 80, 180, " Add another directory ", "")) {
          mousePressed=false;
          messageMain="";
          selectFolder("Select a folder to process:", "folderSelected");
        }
      } else {
        if (button(width/2 -90, 150, 180, " Exit ", ""))
          exit();
        if (button(width/2 -90, 80, 180, " Back ", ""))
        mousePressed=false;
          files2convert = new ArrayList<file2convert>();
      }
    }
    if (convert && !something2do)convert=false;
  }
}