boolean button(int x, int y, int l, String label, String help)
{   
  stroke(255);
  textAlign(CENTER, CENTER);
  if (mouseX<x+l &&mouseX>x &&mouseY<y+30&&mouseY>y) {
    fill(160, 160, 220, 250);
    rect(x, y-5, l, 30, 8);
    
    fill(255); 
    text(label, x, y-5, l, 25);
    textAlign(LEFT, CENTER);
    text(help, x+(l)+20, y+12);
    return mousePressed;
  } else {
    fill(160, 160, 160, 140);
    rect(x, y-5, l, 30, 8); 
    fill(160);
    text(label, x, y-5, l, 25);
    return false;
  }
}
boolean buttonText(int x, int y, int l, String label, String help)
{   
  stroke(255);
  textAlign(CENTER, CENTER);
  if (mouseX<x+l &&mouseX>x &&mouseY<y+30&&mouseY>y) {
    
    fill(255); 
    text(label, x, y-5, l, 25);
    textAlign(LEFT, CENTER);
    text(help, x+(l)+20, y+12);
    return mousePressed;
  } else {
    fill(160);
    text(label, x, y-5, l, 25);
    return false;
  }
}

void linkButton(String label, String url, int x, int y, int l, String help)
{   
  stroke(255);
  textAlign(LEFT);
  if (mouseX<x+l &&mouseX>x &&mouseY<y+30&&mouseY>y) {
    fill(255); 
    text(label, x, y-5, l, 25);
    text(help, x+(l)+20, y+12);
    if (mousePressed)link(url);
  } else { 
    fill(160);
    text(label, x, y-5, l, 25);
  }
}